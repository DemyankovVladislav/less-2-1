#!/bin/bash
curl --request DELETE --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables/DEPLOY"
curl --request DELETE --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables/NPORT"
curl --request DELETE --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables/REMOTE_JOB"
curl --request POST --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables" --form "key=DEPLOY" --form "value=$DEPLOY" | jq .[]
curl --request POST --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables" --form "key=NPORT" --form "value=$NPORT" | jq .[]
curl --request POST --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/variables" --form "key=REMOTE_JOB" --form "value=$REMOTE_JOB" | jq .[]
curl --request POST --header "PRIVATE-TOKEN:$MYTOKEN" --form ref=master "https://gitlab.com/api/v4/projects/$project_id/pipeline" | jq .[]
PIPE_ID=$(curl --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/pipelines" | jq ' .[] | select(.status == "manual") .id' | head -1)
JOB_ID=$(curl --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/pipelines/$PIPE_ID/jobs" | jq ' .[] | select(.name == "test_run") .id')
curl -k --request POST --header "PRIVATE-TOKEN:$MYTOKEN" "https://gitlab.com/api/v4/projects/$project_id/jobs/$JOB_ID/play" | jq .[]  

